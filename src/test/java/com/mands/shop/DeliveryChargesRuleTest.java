package com.mands.shop;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class DeliveryChargesRuleTest {

    @Test
    public void applyNoRanks() throws Exception {
        final DeliveryChargesRule deliveryRule =
                new DeliveryChargesRule(new HashMap<>());

        VerifyDeliveryBasket ruleBasket = new VerifyDeliveryBasket(BigDecimal.ZERO);
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.ZERO, ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(10));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.ZERO, ruleBasket.getDeliveryCost());
    }

    @Test
    public void applySingleRanks() throws Exception {
        final DeliveryChargesRule deliveryRule =
                new DeliveryChargesRule(new HashMap<BigDecimal, BigDecimal>() {
                    {
                        put(BigDecimal.valueOf(50), BigDecimal.valueOf(495, 2));
                    }
                });

        VerifyDeliveryBasket ruleBasket = new VerifyDeliveryBasket(BigDecimal.ZERO);
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.valueOf(495, 2), ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(10));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.valueOf(495, 2), ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(50));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.ZERO, ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(60));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.ZERO, ruleBasket.getDeliveryCost());
    }

    @Test
    public void applyTwoRanks() throws Exception {
        final DeliveryChargesRule deliveryRule =
                new DeliveryChargesRule(new HashMap<BigDecimal, BigDecimal>() {
                    {
                        put(BigDecimal.valueOf(50), BigDecimal.valueOf(495, 2));
                        put(BigDecimal.valueOf(90), BigDecimal.valueOf(295, 2));
                    }
                });

        VerifyDeliveryBasket ruleBasket = new VerifyDeliveryBasket(BigDecimal.ZERO);
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.valueOf(495, 2), ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(10));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.valueOf(495, 2), ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(50));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.valueOf(295, 2), ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(60));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.valueOf(295, 2), ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(90));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.ZERO, ruleBasket.getDeliveryCost());

        ruleBasket = new VerifyDeliveryBasket(BigDecimal.valueOf(100));
        deliveryRule.apply(ruleBasket);
        assertEquals(BigDecimal.ZERO, ruleBasket.getDeliveryCost());
    }

    private static class VerifyDeliveryBasket implements RuleBasket {
        private final BigDecimal cost;
        private BigDecimal deliveryCost;

        public VerifyDeliveryBasket(BigDecimal cost) {
            this.cost = cost;
        }

        @Override
        public void add(ProductInBasket productInBasket) {
            if (productInBasket.getProduct() == DeliveryChargesRule.DELIVERY_PRODUCT && deliveryCost == null) {
                deliveryCost = productInBasket.getActualPrice();
            } else {
                fail();
            }
        }

        @Override
        public void remove(ProductInBasket productInBasket) {

        }

        @Override
        public Map<String, Collection<ProductInBasket>> getAllProductsInBasket() {
            return null;
        }

        @Override
        public RuleBasket clone() {
            return null;
        }

        @Override
        public void add(String productCode) {

        }

        @Override
        public BigDecimal calculateCost() {
            return cost;
        }

        public BigDecimal getDeliveryCost() {
            return deliveryCost;
        }
    }
}