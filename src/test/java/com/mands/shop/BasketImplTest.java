package com.mands.shop;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import static org.junit.Assert.*;

public class BasketImplTest {

    private final ProductCatalog catalog = new ProductCatalogImpl(
            Arrays.asList(
                    new ProductImpl("J01", "Jeans", BigDecimal.valueOf(3295, 2)),
                    new ProductImpl("B01", "Blouse", BigDecimal.valueOf(2495, 2)),
                    new ProductImpl("S01", "Socks", BigDecimal.valueOf(795, 2))
            )
    );

    private final Collection<Rule> rules = Arrays.asList(
            new AnotherItemRule("J01", BigDecimal.valueOf(5, 1)),
            new DeliveryChargesRule(new HashMap<BigDecimal, BigDecimal>() {
                {
                    put(BigDecimal.valueOf(50), BigDecimal.valueOf(495, 2));
                    put(BigDecimal.valueOf(90), BigDecimal.valueOf(295, 2));
                }
            })
    );

    @Test
    public void calculateCost1() throws Exception {
        final Basket basket = new BasketImpl(catalog, rules);
        basket.add("S01");
        basket.add("B01");
        assertEquals(0, BigDecimal.valueOf(3785, 2).compareTo(basket.calculateCost()));
    }

    @Test
    public void calculateCost2() throws Exception {
        final Basket basket = new BasketImpl(catalog, rules);
        basket.add("J01");
        basket.add("J01");
        assertEquals(0, BigDecimal.valueOf(5437, 2).compareTo(basket.calculateCost()));
    }

    @Test
    public void calculateCost3() throws Exception {
        final Basket basket = new BasketImpl(catalog, rules);
        basket.add("J01");
        basket.add("B01");
        assertEquals(0, BigDecimal.valueOf(6085, 2).compareTo(basket.calculateCost()));
    }

    @Test
    public void calculateCost4() throws Exception {
        final Basket basket = new BasketImpl(catalog, rules);
        basket.add("S01");
        basket.add("S01");
        basket.add("J01");
        basket.add("J01");
        basket.add("J01");
        assertEquals(0, BigDecimal.valueOf(9827, 2).compareTo(basket.calculateCost()));
    }
}