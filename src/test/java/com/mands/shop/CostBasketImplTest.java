package com.mands.shop;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

import static org.junit.Assert.*;

public class CostBasketImplTest {

    private final ProductCatalog catalog = new ProductCatalogImpl(
            Arrays.asList(
                    new ProductImpl("J01", "Jeans", BigDecimal.valueOf(3295, 2)),
                    new ProductImpl("B01", "Blouse", BigDecimal.valueOf(2495, 2)),
                    new ProductImpl("S01", "Socks", BigDecimal.valueOf(795, 2))
            )
    );

    @Test(expected = IllegalArgumentException.class)
    public void nullCatalog() throws Exception {
        new CostBasketImpl(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void addIllegalProduct() throws Exception {
        final Basket basket = new CostBasketImpl(catalog);
        basket.add("J02");
    }

    @Test
    public void calculateCostEmpty() throws Exception {
        final Basket basket = new CostBasketImpl(catalog);
        assertEquals(BigDecimal.ZERO, basket.calculateCost());
    }

    @Test
    public void calculateCost() throws Exception {
        final Basket basket = new CostBasketImpl(catalog);
        basket.add("J01");
        basket.add("J01");
        basket.add("B01");
        assertEquals(BigDecimal.valueOf(9085, 2), basket.calculateCost());
    }

    @Test
    public void calculateCostDifferentActualPrice() throws Exception {
        final RuleBasket basket = new CostBasketImpl(catalog);
        basket.add(new ProductInBasketImpl(catalog.get("J01"), BigDecimal.ZERO));
        basket.add("J01");
        basket.add("B01");
        assertEquals(BigDecimal.valueOf(5790, 2), basket.calculateCost());
    }

}