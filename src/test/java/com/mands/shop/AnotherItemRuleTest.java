package com.mands.shop;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;

public class AnotherItemRuleTest {

    private final ProductCatalog catalog = new ProductCatalogImpl(
            Arrays.asList(
                    new ProductImpl("J01", "Jeans", BigDecimal.valueOf(3295, 2)),
                    new ProductImpl("B01", "Blouse", BigDecimal.valueOf(2495, 2)),
                    new ProductImpl("S01", "Socks", BigDecimal.valueOf(795, 2))
            )
    );

    @Test
    public void applyNoProduct() throws Exception {
        final AnotherItemRule itemRule = new AnotherItemRule("B01", BigDecimal.valueOf(5, 1));

        final Map<String, Collection<ProductInBasket>> allProducts = new HashMap<>();
        allProducts.put("J01", Arrays.asList(
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice()),
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice()),
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice()),
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice())
        ));
        final VerifyItemChangeBasket verifyItem = new VerifyItemChangeBasket(allProducts, "B01");
        itemRule.apply(verifyItem);

        assertEquals(0, verifyItem.getAddedProducts().size());
    }

    @Test
    public void applySingleInstance() throws Exception {
        final AnotherItemRule itemRule = new AnotherItemRule("J01", BigDecimal.valueOf(5, 1));

        final Map<String, Collection<ProductInBasket>> allProducts = new HashMap<>();
        allProducts.put("J01", Arrays.asList(
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice())
        ));
        final VerifyItemChangeBasket verifyItem = new VerifyItemChangeBasket(allProducts, "J01");
        itemRule.apply(verifyItem);

        assertEquals(0, verifyItem.getAddedProducts().size());
    }

    @Test
    public void apply() throws Exception {
        final AnotherItemRule itemRule = new AnotherItemRule("J01", BigDecimal.valueOf(5, 1));

        final Map<String, Collection<ProductInBasket>> allProducts = new HashMap<>();
        allProducts.put("J01", Arrays.asList(
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice()),
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice()),
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice()),
                new ProductInBasketImpl(catalog.get("J01"), catalog.get("J01").getPrice())
        ));
        final VerifyItemChangeBasket verifyItem = new VerifyItemChangeBasket(allProducts, "J01");
        itemRule.apply(verifyItem);

        assertEquals(2, verifyItem.getAddedProducts().size());
        assertEquals(BigDecimal.valueOf(1647, 2), verifyItem.getAddedProducts().get(0).getActualPrice());
        assertEquals(BigDecimal.valueOf(1647, 2), verifyItem.getAddedProducts().get(1).getActualPrice());
    }

    private static class VerifyItemChangeBasket implements RuleBasket {
        private final Map<String, Collection<ProductInBasket>> allProducts;
        private final String code;

        private List<ProductInBasket> addedProducts = new ArrayList<>();

        private ProductInBasket removed;

        public VerifyItemChangeBasket(Map<String, Collection<ProductInBasket>> allProducts, String code) {
            this.allProducts = allProducts;
            this.code = code;
        }

        @Override
        public void add(ProductInBasket productInBasket) {
            if (removed == null) {
                fail();
            } else if (!productInBasket.getProduct().getCode().equals(removed.getProduct().getCode())) {
                fail();
            } else {
                addedProducts.add(productInBasket);
                removed = null;
            }
        }

        @Override
        public void remove(ProductInBasket productInBasket) {
            if (productInBasket.getProduct().getCode().equals(code)) {
                removed = productInBasket;
            } else {
                fail();
            }
        }

        @Override
        public Map<String, Collection<ProductInBasket>> getAllProductsInBasket() {
            return allProducts;
        }

        @Override
        public RuleBasket clone() {
            return null;
        }

        @Override
        public void add(String productCode) {

        }

        @Override
        public BigDecimal calculateCost() {
            return null;
        }

        public List<ProductInBasket> getAddedProducts() {
            return addedProducts;
        }
    }
}