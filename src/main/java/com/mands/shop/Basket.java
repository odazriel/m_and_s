package com.mands.shop;

import java.math.BigDecimal;

/**
 * Represents a basket in the shop.
 * The basket holds products and can calculate its own price.
 */
public interface Basket {
    void add(String productCode);
    BigDecimal calculateCost();
}
