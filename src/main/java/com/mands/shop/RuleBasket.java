package com.mands.shop;

import java.util.Collection;
import java.util.Map;

public interface RuleBasket extends Basket, Cloneable {
    void add(ProductInBasket productInBasket);
    void remove(ProductInBasket productInBasket);

    Map<String, Collection<ProductInBasket>> getAllProductsInBasket();

    RuleBasket clone();
}
