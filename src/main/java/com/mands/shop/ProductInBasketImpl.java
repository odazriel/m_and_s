package com.mands.shop;

import java.math.BigDecimal;

class ProductInBasketImpl implements ProductInBasket {

    private final Product product;
    private final BigDecimal actualPrice;

    public ProductInBasketImpl(Product product, BigDecimal actualPrice) {
        if (product == null) {
            throw new IllegalArgumentException("Product must exist");
        } else if (actualPrice == null) {
            throw new IllegalArgumentException("Actual Price must exist");
        } else {
            this.product = product;
            this.actualPrice = actualPrice;
        }
    }

    @Override
    public Product getProduct() {
        return product;
    }

    @Override
    public BigDecimal getActualPrice() {
        return actualPrice;
    }
}
