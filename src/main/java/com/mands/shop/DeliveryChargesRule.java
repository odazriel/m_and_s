package com.mands.shop;

import java.math.BigDecimal;
import java.util.Map;
import java.util.TreeMap;

/**
 * A rule that will add a special "Delivery" product to the basket.
 * The actual cost of the delivery is calculating using (X, Y) pairs where Y is the cost of the delivery for all
 * baskets whose price is under X.
 */
public final class DeliveryChargesRule implements Rule {
    static final Product DELIVERY_PRODUCT = new ProductImpl("##DELIVERY##", "Delivery", BigDecimal.ZERO);

    private final TreeMap<BigDecimal, BigDecimal> deliveryCosts;

    public DeliveryChargesRule(Map<BigDecimal, BigDecimal> deliveryCostByBasketValue) {
        deliveryCosts = new TreeMap<>(deliveryCostByBasketValue);
    }

    @Override
    public void apply(RuleBasket basket) {
        final BigDecimal basketCost = basket.calculateCost();
        BigDecimal addedCost = BigDecimal.ZERO;
        for (final Map.Entry<BigDecimal, BigDecimal> entry : deliveryCosts.entrySet()) {
            if (basketCost.compareTo(entry.getKey()) < 0) {
                addedCost = entry.getValue();
                break;
            }
        }

        basket.add(new ProductInBasketImpl(DELIVERY_PRODUCT, addedCost));
    }

    @Override
    public boolean shouldRunLast() {
        return true;
    }
}
