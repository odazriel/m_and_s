package com.mands.shop;

import java.math.BigDecimal;
import java.util.Collection;

/**
 * A rule that will change the cost of an item if there was another item of the same type.
 * The cost will be changed by multiplying it with a given factor.
 */
public class AnotherItemRule implements Rule {

    private final String productCode;
    private final BigDecimal priceFactor;

    public AnotherItemRule(String productCode, BigDecimal priceFactor) {
        this.productCode = productCode;

        if (priceFactor == null) {
            this.priceFactor = BigDecimal.ONE;
        } else {
            this.priceFactor = priceFactor;
        }
    }

    @Override
    public void apply(RuleBasket ruleBasket) {
        final Collection<ProductInBasket> existingProducts = ruleBasket.getAllProductsInBasket().get(productCode);
        if (existingProducts != null) {
            boolean addAnother = false;
            for (final ProductInBasket original : existingProducts) {
                if (original.getProduct().getCode().equals(productCode)) {
                    if (addAnother) {
                        ruleBasket.remove(original);
                        ruleBasket.add(new ProductInBasketImpl(
                                original.getProduct(),
                                original.getProduct().getPrice().multiply(priceFactor).setScale(2, BigDecimal.ROUND_FLOOR)));
                        addAnother = false;
                    } else {
                        addAnother = true;
                    }
                }
            }
        }
    }

    @Override
    public boolean shouldRunLast() {
        return false;
    }
}
