package com.mands.shop;

public interface ProductCatalog {
    Product get(String productCode);
}
