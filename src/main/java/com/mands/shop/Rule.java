package com.mands.shop;

/**
 * A rule that can change the contents of a basket using the current contents.
 */
public interface Rule {
    void apply(RuleBasket ruleBasket);

    /**
     * Some rules might require all previous rules to have changed the contents already.
     * Such rules should return <code>true</code>, all other rules <code>false</code>
     */
    boolean shouldRunLast();
}
