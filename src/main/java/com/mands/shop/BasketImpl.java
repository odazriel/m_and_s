package com.mands.shop;

import java.math.BigDecimal;
import java.util.*;

/**
 * A basket that is also initialized with a set of rules.
 * When calculating the price of the basket, the rules are taken under consideration.
 */
public class BasketImpl implements Basket {

    private final RuleBasket costBasket;
    private final Collection<Rule> rules = new ArrayList<>();
    private final Collection<Rule> lastRules = new ArrayList<>();

    public BasketImpl(ProductCatalog productCatalog, Collection<Rule> rules) {
        this.costBasket = new CostBasketImpl(productCatalog);
        initializeRuleCollections(rules, this.rules, this.lastRules);
    }

    private void initializeRuleCollections(
            Collection<Rule> originalRules, Collection<Rule> rules, Collection<Rule> lastRules) {
        if (originalRules != null) {
            for (final Rule rule : originalRules) {
                if (rule.shouldRunLast()) {
                    lastRules.add(rule);
                } else {
                    rules.add(rule);
                }
            }
        }
    }

    @Override
    public void add(String productCode) {
        costBasket.add(productCode);
    }

    @Override
    public BigDecimal calculateCost() {
        final RuleBasket basketForCalculation = costBasket.clone();
        applyRules(basketForCalculation);
        return basketForCalculation.calculateCost();
    }

    private void applyRules(RuleBasket basket) {
        rules.forEach(rule -> rule.apply(basket));
        lastRules.forEach(rule -> rule.apply(basket));
    }
}
