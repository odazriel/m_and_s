package com.mands.shop;

import java.math.BigDecimal;

public interface Product {
    String getCode();
    String getName();
    BigDecimal getPrice();
}
