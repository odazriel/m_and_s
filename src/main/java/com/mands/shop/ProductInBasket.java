package com.mands.shop;

import java.math.BigDecimal;

/**
 * Represents a product that is inside a basket.
 * The product's price will always be the same, but the basket might assign a different price for an instnace
 * of a product in it.
 */
public interface ProductInBasket {
    Product getProduct();
    BigDecimal getActualPrice();
}
