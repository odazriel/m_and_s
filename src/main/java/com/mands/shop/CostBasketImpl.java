package com.mands.shop;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A basket that can have rules applied to it.
 * The rules change the actual content of the basket.
 */
final class CostBasketImpl implements RuleBasket, Cloneable {

    private final ProductCatalog productCatalog;
    private final Map<String, Collection<ProductInBasket>> productsInBasket = new HashMap<>();

    public CostBasketImpl(ProductCatalog productCatalog) {
        if (productCatalog == null) {
            throw new IllegalArgumentException("Product catalog must not be null");
        } else {
            this.productCatalog = productCatalog;
        }
    }

    private CostBasketImpl(CostBasketImpl other) {
        this.productCatalog = other.productCatalog;
        this.productsInBasket.putAll(other.productsInBasket);
    }

    @Override
    public void add(String productCode) {
        final Product product = productCatalog.get(productCode);
        if (product == null) {
            throw new IllegalArgumentException(String.format("Product Code [%s] does not exist in catalog", productCode));
        } else {
            add(new ProductInBasketImpl(product, product.getPrice()));
        }
    }

    @Override
    public BigDecimal calculateCost() {
        return calculateOriginalProductCost();
    }

    private BigDecimal calculateOriginalProductCost() {
        return productsInBasket
                .values()
                .stream()
                .flatMap(Collection::stream)
                .map(ProductInBasket::getActualPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    @Override
    public void add(ProductInBasket product) {
        final String code = product.getProduct().getCode();
        if (!productsInBasket.containsKey(code)) {
            productsInBasket.put(code, new ArrayList<>());
        }
        productsInBasket.get(code).add(product);
    }

    @Override
    public void remove(ProductInBasket productInBasket) {
        final Collection<ProductInBasket> products = productsInBasket.get(productInBasket.getProduct().getCode());
        if (products != null) {
            products.remove(productInBasket);
        }
    }

    @Override
    public Map<String, Collection<ProductInBasket>> getAllProductsInBasket() {
        return productsInBasket.entrySet()
                .stream()
                .collect(Collectors.toMap(Map.Entry::getKey, entry -> new ArrayList<>(entry.getValue())));
    }

    @Override
    public CostBasketImpl clone() {
        return new CostBasketImpl(this);
    }
}