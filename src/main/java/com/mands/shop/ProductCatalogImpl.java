package com.mands.shop;

import java.util.Collection;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ProductCatalogImpl implements ProductCatalog {

    private Map<String, Product> products;

    public ProductCatalogImpl(Collection<Product> products) {
        this.products = products.stream().collect(Collectors.toMap(Product::getCode, Function.identity()));
    }

    @Override
    public Product get(String productCode) {
        return products.get(productCode);
    }
}
