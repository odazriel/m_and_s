package com.mands.shop;

import java.math.BigDecimal;

public class ProductImpl implements Product {
    private final String code;
    private final String name;
    private final BigDecimal price;

    public ProductImpl(String code, String name, BigDecimal price) {
        this.code = code;
        this.name = name;
        this.price = price;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public BigDecimal getPrice() {
        return price;
    }
}
